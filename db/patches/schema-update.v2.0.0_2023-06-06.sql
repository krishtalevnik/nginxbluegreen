-- -----------------------------------------------------
-- Update to Table `team`
-- -----------------------------------------------------

ALTER TABLE `team`
  MODIFY COLUMN api_managed_roster BOOLEAN NOT NULL DEFAULT FALSE;
