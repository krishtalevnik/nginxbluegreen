-- -----------------------------------------------------
-- Update to Table `team`
-- -----------------------------------------------------

ALTER TABLE `team` 
  MODIFY COLUMN slack_channel_notifications VARCHAR(255);
