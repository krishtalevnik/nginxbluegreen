#!/bin/bash
##########################################
blue="/etc/nginx/switch/blue"
green="/etc/nginx/switch/green"

# Functions
reloadNg (){
  nginx -s reload
}

switch2Green () {
  echo "Switching to: green"
  echo $green
  rm -rf $blue
  touch $green
  reloadNg
  exit 0
}

switch2Blue () {
  echo $blue
  echo "Switching to: blue"
  rm -rf $green
  touch $blue
  reloadNg
  exit 0
}

findActive () {
  if [ -e $blue ]; then
    echo "ActiveSite:blue"
    switch2Green
  fi

  if [ -e $green ]; then
    echo "ActiveSite:green"
    switch2Blue
  fi

  echo "No active site!"
  switch2Blue
}

# Now some action
findActive