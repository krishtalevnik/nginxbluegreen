#!/bin/bash
function checkIfV1Up () {
  isUp=false
  tries=1
  while [ $tries -le 5 ]
  do
    status=$(docker compose ps --status=running oncall-web);
    if [[ $status =~ .*"oncall-web"*. ]]; then
      isUp=true
      break
    else
      x=$(( $x + 1 ))
      sleep 2
    fi
  done
  echo $isUp
}

function checkIfV2Up () {
  isUp=false
  tries=1
  while [ $tries -le 5 ]
  do
    status=$(docker compose ps --status=running oncall-web-v2);
    if [[ $status =~ .*"oncall-web-v2"*. ]]; then
      isUp=true
      break
    else
      x=$(( $x + 1 ))
      sleep 2
    fi
  done
  echo $isUp
}

function upgradeV1() {
    docker-compose down oncall-web
    sleep 10
    docker-compose up -d oncall-web
}

function downgradeV1() {
    docker-compose up -d oncall-web
}

function stopV2() {
    docker-compose down oncall-web-v2;
}

function upgradeV2() {
    docker-compose up -d oncall-web-v2;
}

function switch() {
    docker-compose exec nginx-entrypoint bash /etc/nginx/script.sh
}

upgradeV2
isUpV2=$(checkIfV2Up)

if ($isUpV2 == true); then
  switch
  upgradeV1
  isUpV1=$(checkIfV1Up)
  if ($isUpV2 == true); then
    switch
    stopV2
  else
    downgradeV1
    switch
  fi
fi
